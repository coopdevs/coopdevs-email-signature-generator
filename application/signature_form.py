# -*- coding: utf-8 -*-
from wtforms import Form, BooleanField, StringField, PasswordField, validators
# from wtforms.fields.html5 import EmailField


class SignatureForm(Form):
    username = StringField('El teu nom complet', [validators.DataRequired(), validators.Length(min=4, max=25)])
    userphone = StringField(u'El teu telèfon (sense prefix ni espais) exemple: 654842056')
    useremail = StringField('Email Address', [validators.DataRequired()])
    RGPD = BooleanField(label="RGPD" )
    publi = BooleanField(label="Envíos publicitarios" )