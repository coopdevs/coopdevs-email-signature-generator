from setuptools import setup

setup(
    name='signatura',
    packages=['application'],
    include_package_data=True,
    install_requires=[
        'flask',
        'click',
        'Flask',
        'itsdangerous',
        'Jinja2',
        'MarkupSafe',
        'Werkzeug',
        'wtforms'
    ],
)
